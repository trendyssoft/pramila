/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package vidco;

import com.sun.awt.AWTUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.Timer;

/**
 *
 * @author Susantha Madushan
 */
public class first extends javax.swing.JFrame {

    /**
     * Creates new form first
     */
    Timer ttt;
    SimpleDateFormat sum;
    String d, s1;

    public first() {
        initComponents();
        try {
            Date dt = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            d = sdf.format(dt);
            Date.setText(d);

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ttt = new javax.swing.Timer(1000, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    sum = new SimpleDateFormat("hh:mm:ss:a");
                    Date d = new Date();
                    s1 = sum.format(d);
                    time.setText(s1);
                }
            });
            ttt.start();
        } catch (Exception e) {
        }
        AWTUtilities.setWindowOpaque(this, false);
        new Thread() {
            public void run() {


                for (int i = 0; i < 101; i += 2) {
                    try {
                        sleep(120);
                        jProgressBar1.setValue(i);

                        if (jProgressBar1.getValue() <= 00) {
                            jLabel3.setText("          Loading.");
                        } else if (jProgressBar1.getValue() <= 5) {
                            jLabel3.setText("          Loading..");
                        } else if (jProgressBar1.getValue() <= 10) {
                            jLabel3.setText("          Loading...");
                        } else if (jProgressBar1.getValue() <= 15) {
                            jLabel3.setText("          Loading....");
                        } else if (jProgressBar1.getValue() <= 20) {
                            jLabel3.setText("          Loading.....");
                        } else if (jProgressBar1.getValue() <= 25) {
                            jLabel3.setText("          Loading.");
                        } else if (jProgressBar1.getValue() <= 30) {
                            jLabel3.setText("          Loading..");
                        } else if (jProgressBar1.getValue() <= 35) {
                            jLabel3.setText("          Loading...");
                        } else if (jProgressBar1.getValue() <= 40) {
                            jLabel3.setText("          Loading....");
                        } else if (jProgressBar1.getValue() <= 45) {
                            jLabel3.setText("          Loading.....");
                        } else if (jProgressBar1.getValue() <= 50) {
                            jLabel3.setText("          Loading.");
                        } else if (jProgressBar1.getValue() <= 55) {
                            jLabel3.setText("          Loading..");
                        } else if (jProgressBar1.getValue() <= 60) {
                            jLabel3.setText("          Loading...");
                        } else if (jProgressBar1.getValue() <= 65) {
                            jLabel3.setText("          Loading....");
                        } else if (jProgressBar1.getValue() <= 70) {
                            jLabel3.setText("Making Data Base Backup.....");
                            try {
//            String tim=Time.getText();
//            System.out.println(tim);
                                String s = s1.replace(":", "-");
                                String dt = "SQLBackup" + d +"-"+ s;

                                String sql = "C:\\Program Files (x86)\\MySQL\\MySQL Server 5.1\\bin\\mysqldump -uroot -p123 vidcodb -r E:\\VidcoBackUp\\" + dt + ".sql";
                                Runtime.getRuntime().exec(sql);
                                BackUpOutTxt.setText("E:\\VidcoBackUp\\" + dt + ".sql");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }  else if (jProgressBar1.getValue() <= 80) {
                            jLabel3.setText("          Loading..");
                            BackUpOutTxt.setText(BackUpOutTxt.getText());
                        } else if (jProgressBar1.getValue() <= 85) {
                            jLabel3.setText("          Loading...");
                            BackUpOutTxt.setText(BackUpOutTxt.getText());
                        } else if (jProgressBar1.getValue() <= 90) {
                            jLabel3.setText("          Loading....");
                        } else if (jProgressBar1.getValue() <= 95) {
                            jLabel3.setText("          Loading.....");
                        } else {
                            jLabel3.setText("");

                        }




                    } catch (InterruptedException e) {
                        e.printStackTrace();

                    }



                }


                maaaaain m = new maaaaain();
                m.setVisible(true);
                dispose();



            }
        }.start();


    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        BackUpOutTxt = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        Date = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BackUpOutTxt.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        BackUpOutTxt.setForeground(new java.awt.Color(204, 204, 204));
        jPanel1.add(BackUpOutTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 450, 390, 20));

        time.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        time.setForeground(new java.awt.Color(204, 0, 0));
        jPanel1.add(time, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 50, 180, 20));

        Date.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        Date.setForeground(new java.awt.Color(204, 0, 0));
        jPanel1.add(Date, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, 180, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 400, 310, 40));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/525416_338953402890988_2012006960_n.png"))); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, 370, 290));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/glass.png"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 50, -1, 403));

        jProgressBar1.setStringPainted(true);
        jPanel1.add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 360, 320, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 880, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 485, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(880, 485));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(first.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new first().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackUpOutTxt;
    private javax.swing.JLabel Date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JLabel time;
    // End of variables declaration//GEN-END:variables
}
