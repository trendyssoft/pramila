package vidco;

import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author Susantha Madushan
 */
public class coursedetails extends javax.swing.JFrame {

    /**
     * Creates new form
     */
    public coursedetails() {
        initComponents();
        setSize(Toolkit.getDefaultToolkit().getScreenSize());

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtid = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtname = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtschool = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        combosubject = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablesub = new javax.swing.JTable();
        txtsubid = new javax.swing.JTextField();
        name = new javax.swing.JTextField();
        txtprice = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        labpri = new javax.swing.JLabel();
        labsub = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtdis = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        labbal = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        save1 = new javax.swing.JButton();
        duedate = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txtpayid = new javax.swing.JTextField();
        txtcash = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel2.setText("Student Course Registration & Payments");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 30, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Student ID");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 120, -1, -1));

        txtid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtidActionPerformed(evt);
            }
        });
        getContentPane().add(txtid, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 120, 200, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Student Name");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 160, -1, -1));

        txtname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnameActionPerformed(evt);
            }
        });
        getContentPane().add(txtname, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 160, 200, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("School ");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 200, -1, -1));
        getContentPane().add(txtschool, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 200, 200, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Grade");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 270, -1, -1));

        combosubject.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Grade 3", "Grade 4", "Grade 5", "Grade 6", "Grade 7", "Grade 8", "Grade 9", "Grade 10", "Grade 11" }));
        combosubject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combosubjectActionPerformed(evt);
            }
        });
        getContentPane().add(combosubject, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 270, 200, -1));

        tablesub.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Subject ID", "Grade", "Subject Name", "Price"
            }
        ));
        tablesub.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablesubMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablesub);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 320, 560, 360));

        txtsubid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtsubidActionPerformed(evt);
            }
        });
        getContentPane().add(txtsubid, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 190, 220, -1));

        name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameActionPerformed(evt);
            }
        });
        getContentPane().add(name, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 290, 220, -1));
        getContentPane().add(txtprice, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 340, 220, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Fee");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 340, -1, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Subject ID");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 190, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Subject Name");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 290, -1, -1));

        labpri.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        labpri.setForeground(new java.awt.Color(204, 0, 0));
        labpri.setText("0");
        getContentPane().add(labpri, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 460, 160, 50));

        labsub.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        labsub.setForeground(new java.awt.Color(204, 0, 0));
        labsub.setText("0");
        getContentPane().add(labsub, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 520, 160, 50));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Balance");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 650, -1, -1));

        txtdis.setText("0");
        txtdis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdisActionPerformed(evt);
            }
        });
        getContentPane().add(txtdis, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 400, 140, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("RS.");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 630, -1, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("Discounts");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 410, -1, -1));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("Price");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 470, -1, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Cash");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 590, -1, -1));

        labbal.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        labbal.setForeground(new java.awt.Color(0, 153, 51));
        labbal.setText("0");
        getContentPane().add(labbal, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 620, 140, 40));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setText("Discounted Price");
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 540, -1, -1));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("/100");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 410, -1, -1));

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("RS.");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 480, -1, -1));

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setText("RS.");
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 540, -1, -1));

        save1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save (4).png"))); // NOI18N
        save1.setText("SAVE");
        save1.setContentAreaFilled(false);
        save1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        save1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save (5).png"))); // NOI18N
        save1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                save1ActionPerformed(evt);
            }
        });
        getContentPane().add(save1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1150, 200, 160, 60));

        duedate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                duedateActionPerformed(evt);
            }
        });
        getContentPane().add(duedate, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 140, 220, -1));

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Payment Due Date");
        getContentPane().add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 140, -1, 20));

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Payment ID");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 240, -1, -1));

        txtpayid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpayidActionPerformed(evt);
            }
        });
        getContentPane().add(txtpayid, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 240, 220, -1));

        txtcash.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcashActionPerformed(evt);
            }
        });
        txtcash.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcashKeyReleased(evt);
            }
        });
        getContentPane().add(txtcash, new org.netbeans.lib.awtextra.AbsoluteConstraints(840, 580, 230, -1));

        jLabel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 90, 410, 590));

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Logout(4).png"))); // NOI18N
        jButton6.setText("Log Out");
        jButton6.setContentAreaFilled(false);
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton6.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Logout(5).png"))); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 610, 160, 60));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/background.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1360, 770));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtidActionPerformed

        try {
            Statement s = DB.vidConnection().createStatement();
            ResultSet rs = s.executeQuery("select * from studentdetails where studentid='" + txtid.getText() + "'");
            while (rs.next()) {
                txtname.setText(rs.getString("fullname"));
                txtschool.setText(rs.getString("school"));

                txtname.grabFocus();


            }


        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }



    }//GEN-LAST:event_txtidActionPerformed

    private void txtnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnameActionPerformed
        txtschool.grabFocus();
    }//GEN-LAST:event_txtnameActionPerformed

    private void combosubjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combosubjectActionPerformed
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject3 where grade='" + combosubject.getSelectedItem() + "' ");

            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);



            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }

        subject4();
        subject5();
        subject6();
        subject7();
        subject8();
        subject9();
        subject10();
        subject11();
    }//GEN-LAST:event_combosubjectActionPerformed

    private void tablesubMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablesubMouseClicked
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel dt = (DefaultTableModel) tablesub.getModel();

            Vector v = new Vector();

            int a = tablesub.getSelectedRow();
            String s1 = dt.getValueAt(a, 0).toString();
            String s2 = dt.getValueAt(a, 2).toString();
            String s3 = dt.getValueAt(a, 3).toString();



            txtsubid.setText(s1);
            name.setText(s2);
            txtprice.setText(s3);

            Integer pri = Integer.parseInt(txtprice.getText());
            Integer labp = Integer.parseInt(labpri.getText());

            Integer tot = (pri + labp);
            labpri.setText(tot.toString());

            duedate.grabFocus();

            s.executeUpdate("insert into subject values('" + txtid.getText() + "','" + combosubject.getSelectedItem() + "','" + name.getText() + "') ");








        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_tablesubMouseClicked

    private void txtdisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdisActionPerformed
        Integer dis = Integer.parseInt(txtdis.getText());
        // Integer pri = Integer.parseInt(txtprice.getText());
        Integer labp = Integer.parseInt(labpri.getText());

        Integer fi = (labp * dis);
        Integer ff = fi / 100;

        Integer t = labp - ff;
        labsub.setText(t.toString());

        txtcash.grabFocus();







    }//GEN-LAST:event_txtdisActionPerformed

    private void save1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_save1ActionPerformed
        try {
            Statement s = DB.vidConnection().createStatement();
            s.executeUpdate("insert into payments values('" + txtid.getText() + "','" + txtsubid.getText() + "','" + txtname.getText() + "','" + combosubject.getSelectedItem() + "','" + labsub.getText() + "','" + txtpayid.getText() + "','"+duedate.getText()+"')");

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_save1ActionPerformed

    private void txtpayidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpayidActionPerformed
        txtdis.grabFocus();
                
        
    }//GEN-LAST:event_txtpayidActionPerformed

    private void txtcashKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcashKeyReleased
        Integer cas = Integer.parseInt(txtcash.getText());
        Integer di = Integer.parseInt(labsub.getText());

        Integer ba = cas - di;
        labbal.setText(ba.toString());
    }//GEN-LAST:event_txtcashKeyReleased

    private void txtcashActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcashActionPerformed
        save1.grabFocus();
    }//GEN-LAST:event_txtcashActionPerformed

    private void nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameActionPerformed
        try {
            
            Statement s = DB.vidConnection().createStatement();
            s.executeUpdate("insert into subject values('" + txtid.getText() + "','" + combosubject.getSelectedItem() + "','" + name.getText() + "','"+ duedate.getText()+ "')");

            
                   

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_nameActionPerformed

    private void duedateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_duedateActionPerformed
       String ss =  duedate.getText();
        if (ss.isEmpty()) {
            JOptionPane.showMessageDialog(this,"Fields can't be Empty!");
                    
            
        } else {
            txtpayid.grabFocus();
        }
        
                
    }//GEN-LAST:event_duedateActionPerformed

    private void txtsubidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtsubidActionPerformed
        
    }//GEN-LAST:event_txtsubidActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        maaaaain m = new maaaaain();
        m.setVisible(true);
        dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(coursedetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(coursedetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(coursedetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(coursedetails.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new coursedetails().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox combosubject;
    private javax.swing.JTextField duedate;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labbal;
    private javax.swing.JLabel labpri;
    private javax.swing.JLabel labsub;
    private javax.swing.JTextField name;
    private javax.swing.JButton save1;
    private javax.swing.JTable tablesub;
    private javax.swing.JTextField txtcash;
    private javax.swing.JTextField txtdis;
    private javax.swing.JTextField txtid;
    private javax.swing.JTextField txtname;
    private javax.swing.JTextField txtpayid;
    private javax.swing.JTextField txtprice;
    private javax.swing.JTextField txtschool;
    private javax.swing.JTextField txtsubid;
    // End of variables declaration//GEN-END:variables

    private void subject4() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject4 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject5() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject5 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject6() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject6 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject7() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject7 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject8() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject8 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject9() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject9 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject10() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject10 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void subject11() {
        try {
            Statement s = DB.vidConnection().createStatement();
            DefaultTableModel df = (DefaultTableModel) tablesub.getModel();
            ResultSet rs = s.executeQuery("select * from subject11 where grade='" + combosubject.getSelectedItem() + "' ");
            while (rs.next()) {
                Vector v = new Vector();

                v.add(rs.getString(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                df.addRow(v);
            }

        } catch (Exception ex) {
            Logger.getLogger(coursedetails.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
